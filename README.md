This is a very small, but helpful postgresql backup script/container with housekeeping included.
It is dumping your databases and pushes them to a S3 storage of choice. It keeps the last X backups and automatically deletes everything older

# How to run
you can use the public docker container and run it for example in Kubernetes as cron-job.
```
docker pull registry.gitlab.com/trieb.work/postgres-backup-s3
```
You need to provide the following environment variables:
```
S3_KEY
S3_SECRET
S3_ENDPOINT
S3_BUCKET_NAME
POSTGRES_DB_HOST
POSTGRES_DB_PORT
POSTGRES_DB_USER
POSTGRES_DB_PASSWORD
DATABASES
NUMBER_OF_VERSIONS
```